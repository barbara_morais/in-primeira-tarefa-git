git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init - cria um repositório GIT em determinado diretório, esse repositório armazenará os códigos refentes ao projeto.

git status - rastreia e verifica se há alguma branch com alteração que não foi atualizada 

git add arquivo/diretório - adiciona o arquivo determinado com as alterações realizadas na área de preparação para dar commit, indicando que os mesmos serão rastreados
git add --all = git add . - adiciona todos os arquivos com as alterações realizadas para dar commit

git commit -m “Primeiro commit” -  salva as mudanças no repositório local e adiciona uma breve descrição referente ao que foi modificado na atualização

-------------------------------------------------------

git log - mostra o histórico das modificações realizadas, mostrando nome, commit, data e e-mail 
git log arquivo - mostra as alterações feitas em determinado arquivo
git reflog - mostra as alterações feitas no projeto, porém de forma mais resumida

-------------------------------------------------------

git show - exibe vários tipos de objetos
git show <commit> - exibe a mensagem do último registro log e a diferença textual

-------------------------------------------------------

git diff - compara o que está na área de TRABALHO (estado modificado) com o commit mais recente
git diff <commit1> <commit2> - compara o que está na área de PREPARAÇÃO (estado preparado) com commit mais recente

-------------------------------------------------------

git reset --hard <commit> - desfaz todas as alterações que aconteceram em arquivos rastreados

-------------------------------------------------------

git branch - lista as branchs locais existentes na pasta
git branch -r - lista todas as branches remotas	
git branch -a - lista todas as branches, tanto locais quanto remotas.
git branch -d <branch_name> - remove a branch local indicada
git branch -D <branch_name> - faz uma remoção forçada de uma branch local, pois muitas vezes a -d não remove
git branch -m <nome_novo> - renomeia a branch atual para um novo_nome
git branch -m <nome_antigo> <nome_novo> - renomeia a branch de nome_antigo para novo_nome

-------------------------------------------------------

git checkout <branch_name> - muda o código para a branch determinada 
git checkout -b <branch_name> - cria uma nova branch e troca o versionamento para a mesma, ou seja, abre o código dela

-------------------------------------------------------

git merge <branch_name> - faz a mesclagem das mudanças na branch indicada com a corrente

-------------------------------------------------------

git clone - cria uma cópia do repositório para a pasta corrente, podendo ser uma pasta local ou url
git pull - atualiza o repositório local com a última versão salva da branch remota
git push - envia as mudanças commitadas localmente para a origem da branch rastreada

-------------------------------------------------------

git remote -v - lista os servidores remotos que o repositório está usando associados com a URL
git remote add origin <url> - é usado para adicionar uma nova origem ou um novo controle remoto
git remote <url> origin - troca a URL do servidor 'origin' para a nova URL informada

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s


